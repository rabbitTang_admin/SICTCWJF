package com.etang.sictcwjf;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WeleComeActivity extends AppCompatActivity {

    private Button btn_welecome_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welecome);
        initView();
        btn_welecome_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initView() {
        btn_welecome_next = (Button) findViewById(R.id.btn_welecome_next);
    }
}