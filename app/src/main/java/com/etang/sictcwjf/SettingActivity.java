package com.etang.sictcwjf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.etang.sictcwjf.tools.WebActivity;
import com.etang.sictcwjf.tools.dialog.CheckUpdateDialog;
import com.etang.sictcwjf.tools.dialog.DeBugDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.etang.sictcwjf.databinding.ActivitySettingBinding;

public class SettingActivity extends AppCompatActivity {

    private TextView tv_title_text, tv_title_button;
    private LinearLayout lv_back, lv_uninstall_setting, lv_gonggao_setting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initView();
        lv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tv_title_text.setText("设置");
        tv_title_button.setText("检查更新");
        tv_title_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckUpdateDialog.check_update(SettingActivity.this, SettingActivity.this, "about");
            }
        });
        lv_uninstall_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UninstallApk(SettingActivity.this, SettingActivity.this, getPackageName());
            }
        });
        lv_gonggao_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SettingActivity.this, WebActivity.class));
            }
        });
    }

    private void initView() {
        lv_back = (LinearLayout) findViewById(R.id.lv_back);
        tv_title_button = (TextView) findViewById(R.id.tv_title_button);
        tv_title_text = (TextView) findViewById(R.id.tv_title_text);
        lv_uninstall_setting = (LinearLayout) findViewById(R.id.lv_uninstall_setting);
        lv_gonggao_setting = (LinearLayout) findViewById(R.id.lv_gonggao_setting);
    }


    /**
     * 唤起系统的卸载apk功能
     */
    private void UninstallApk(Context context, Activity activity, String pakename) {
        try {
            Uri packageURI = Uri.parse("package:" + pakename);
            Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
            activity.startActivityForResult(uninstallIntent, 1);
        } catch (Exception e) {
            DeBugDialog.debug_show_dialog(context, e.toString(), "");
        }
    }

}