package com.etang.sictcwjf;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private WebView wv_index;
    private ProgressBar pg_top;
    private Button btn_sys_exit, btn_sys_reload, btn_sys_setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //冷启动
        cold_start();
        //绑定控件
        initView();
        //检查屏幕状态
        getOrientation(MainActivity.this);
        //检查是否第一次启动
        initFirst();
        //设置并加载webview（腾讯H5）
        web_setting();
        //刷新按钮
        btn_sys_reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wv_index.reload();
                Log.e("URL", wv_index.getUrl());
            }
        });
        //安全退出
        btn_sys_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wv_index.clearCache(true);
                System.exit(0);
            }
        });
        btn_sys_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
            }
        });
    }

    private void initFirst() {
        SharedPreferences preferences = getSharedPreferences(
                "SHARE_APP_TAG", 0);
        Boolean isFirst = preferences.getBoolean("FIRSTStart", true);
        if (isFirst) {// 第一次
            preferences.edit().putBoolean("FIRSTStart", false).commit();
            startActivity(new Intent(MainActivity.this, WeleComeActivity.class));
        } else {
        }
    }

    private void initView() {
        wv_index = (WebView) findViewById(R.id.wv_index);
        pg_top = (ProgressBar) findViewById(R.id.progressBar1);
        btn_sys_exit = (Button) findViewById(R.id.btn_sys_exit);
        btn_sys_reload = (Button) findViewById(R.id.btn_sys_reload);
        btn_sys_setting = (Button) findViewById(R.id.btn_sys_setting);
    }

    private void web_setting() {
        WebSettings seting = wv_index.getSettings();
        /**
         * 必须打开的设置
         */
        seting.setJavaScriptEnabled(true);//设置webview支持javascript脚本
        seting.setDomStorageEnabled(true);
        seting.setJavaScriptCanOpenWindowsAutomatically(true);
        seting.setDisplayZoomControls(true);
        wv_index.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    pg_top.setVisibility(View.GONE);//加载完网页进度条消失
                } else {
                    pg_top.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    pg_top.setProgress(newProgress);//设置进度值
                }
            }
        });
        wv_index.loadUrl("http://cwjf.sict.edu.cn/");
        wv_index.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //使用WebView加载显示url
                view.loadUrl(url);
                //返回true
                return false;
            }
        });
        wv_index.setWebViewClient(new WebViewClient() {
            @Override
            public void onLoadResource(WebView view, String url) {
                Log.e("hao", "WebView3:" + view.getUrl() + "\\n" + "   URL3:" + url);
                super.onLoadResource(view, url);
            }
        });
    }

    private void cold_start() {
        // 在调用TBS初始化、创建WebView之前进行如下配置
        HashMap map = new HashMap();
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_SPEEDY_CLASSLOADER, true);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_DEXLOADER_SERVICE, true);
        QbSdk.initTbsSettings(map);
    }

    //设置返回键动作（防止按返回键直接退出程序)
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO 自动生成的方法存根
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (wv_index.canGoBack()) {//当webview不是处于第一页面时，返回上一个页面
                wv_index.goBack();
                return true;
            } else {//当webview处于第一页面时,直接退出程序
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setCancelable(false);
                builder.setTitle("提示");
                builder.setMessage("确定要退出吗？（退出不会清空登录数据，需要安全退出请点击右上角。）");
                builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.exit(0);
                    }
                });
                builder.setNeutralButton("取消", null);
                builder.show();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /* *
     * 屏幕旋转时调用此方法
     * */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        getOrientation(MainActivity.this);
        super.onConfigurationChanged(newConfig);
    }

    /**
     * 检查屏幕状态
     *
     * @param ctx
     */
    private void getOrientation(Context ctx) {
        if (ctx.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //竖屏
            wv_index.getSettings().setUserAgentString("Android");
            wv_index.reload();
        } else {
            //横屏
            wv_index.getSettings().setUserAgentString("PC");
            wv_index.reload();
        }
    }

}