package com.etang.sictcwjf.tools;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.etang.sictcwjf.R;
import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import java.util.HashMap;

public class WebActivity extends Activity {

    private WebView wv_other;
    private ProgressBar pg_other;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        initView();
        web_setting();
    }

    private void initView() {
        pg_other = (ProgressBar) findViewById(R.id.pg_other);
        wv_other = (WebView) findViewById(R.id.wv_other);
    }

    private void web_setting() {
        WebSettings seting = wv_other.getSettings();
        /**
         * 必须打开的设置
         */
        seting.setJavaScriptEnabled(true);//设置webview支持javascript脚本
        seting.setDomStorageEnabled(true);
        seting.setJavaScriptCanOpenWindowsAutomatically(true);
        seting.setDisplayZoomControls(true);
        wv_other.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    pg_other.setVisibility(View.GONE);//加载完网页进度条消失
                } else {
                    pg_other.setVisibility(View.VISIBLE);//开始加载网页时显示进度条
                    pg_other.setProgress(newProgress);//设置进度值
                }
            }
        });
        wv_other.loadUrl("http://cwjf.sict.edu.cn/aAppPage/index.aspx#/homeMain/xtggSec/bm");
        wv_other.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //使用WebView加载显示url
                view.loadUrl(url);
                //返回true
                return true;
            }
        });
    }

    private void cold_start() {
        // 在调用TBS初始化、创建WebView之前进行如下配置
        HashMap map = new HashMap();
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_SPEEDY_CLASSLOADER, true);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_DEXLOADER_SERVICE, true);
        QbSdk.initTbsSettings(map);
    }

}
