package com.etang.sictcwjf.tools.util;


import ru.alexbykov.nopermission.BuildConfig;

/**
 * @Package: com.etang.nt_launcher.tool.util
 * @ClassName: MTCore
 * @Description: 存放整个APP能用到的变量
 * @CreateDate: 2021/3/19 8:29
 * @UpdateDate: 2021/3/19 8:29
 */
public class MTCore {
    //微信推送KEY
    public static String SERVER_WeChat_URL_KEY = "SCT42251T0LTbGyWytVR7OQqmFZaLlTNO";
    //微信推送URL
    public static String SERVER_WeChat_URL = "sctapi.ftqq.com";
    //学校各个站点
    //域名
    public static String Url_web = "http://cwjf.sict.edu.cn/";
    //首页
    public static String Url_index = "http://cwjf.sict.edu.cn/";
}
