## SICTCWJF

 - 开源地址：[奶油话梅糖/SICTCWJF (gitee.com)](https://gitee.com/rabbitTang_admin/SICTCWJF)
 - 下载地址：https://yp.nyanon.online/index.php?share/file&user=1&sid=ZQDsqchP

> 基于WebView的山东商业职业技术学院财务缴费系统制作的APP

## 截图

| 欢迎界面                                                     | 登陆界面                                                     | 设置界面                                                     | 软件更新                                                     | 公告界面                                                     | 缴费首页                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-31-17-23-44-020_%E5%95%86%E8%81%8C%E7%BC%B4%E8%B4%B9.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-31-17-23-54-173_%E5%95%86%E8%81%8C%E7%BC%B4%E8%B4%B9.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-31-17-23-59-922_%E5%95%86%E8%81%8C%E7%BC%B4%E8%B4%B9.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-31-17-24-08-216_%E5%95%86%E8%81%8C%E7%BC%B4%E8%B4%B9.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/Screenshot_2021-08-31-17-24-20-126_%E5%95%86%E8%81%8C%E7%BC%B4%E8%B4%B9.png) | ![](https://rabbittang_admin.gitee.io/gallery/Screenshots/%E6%B6%82%E9%B8%A6_Screenshot_2021-08-31-17-34-53-525_%E5%95%86%E8%81%8C%E7%BC%B4%E8%B4%B9.png) |

## 前言

> 请根据您的语言点击相对应的连接来查看不同语言的README.md
>
> Please click the corresponding link according to your language to view the README.md in different languages.

| 语言    | 版本 | 链接 |
| ------- | ---- | ---- |
| English | 暂无 | 暂无 |
| 中文    | 1.0  | 当前 |
| 日本語  | 暂无 | 暂无 |

#### 更新地址

- 最新更新在：[Gitee码云](https://gitee.com/rabbitTang_admin/S.H.Home-Project-Reset)
- 我的博客是：[どうして私だけが](https://blog.nyanon.online/)

## 介绍

### 关于开源

本项目已与``2019年12月01日``开源，采用**[Apache License 2.0](https://gitee.com/rabbitTang_admin/NT-Eink-Launcher/blob/master/LICENSE)**许可证，各取所需。

### 关于跨平台

~~本项目已通过`Kotlin`混合编程实现跨平台，理论支持Mac、Linux、Windows等平台~~

暂时放弃跨平台（很多API需要重写才能实现功能，日后有空再维护）

### 用户文档

编写中

## 赞赏

| 微信                                                         | 支付宝                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| ![微信](https://rabbittang_admin.gitee.io/gallery/pay_core/wechatpay.jpg) | ![支付宝](https://rabbittang_admin.gitee.io/gallery/pay_core/alipay.jpg) |

 